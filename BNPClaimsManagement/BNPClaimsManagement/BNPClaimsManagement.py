﻿#########################
### to get about .4548
#################

import os
import sklearn
import pandas as pd
import numpy as np
import csv
from sklearn import ensemble
from sklearn import linear_model
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import log_loss
from sklearn import cross_validation
import xgboost as xgb
from mpmath import matrix
from dynd.nd.array import zeros
from sklearn.grid_search import GridSearchCV

def filter_features(train, test):
    #featuresNames = ['v3','v10','v12','v14','v21','v22','v24','v30','v31','v34','v38','v40','v47','v50','v52','v56','v62','v66','v71','v72','v74','v75','v79','v91','v107','v110','v112','v113','v114','v125','v129']
    uselessFeatures = ['v1','v2','v4','v5','v6','v7','v8','v9','v11','v13','v15','v16','v17','v18','v19','v20','v23','v25','v26','v27','v28','v29','v32','v33','v35','v36',
                       'v37','v39','v41','v42','v43','v44','v45','v46','v48','v49','v51','v53','v54','v55','v57','v58','v59','v60','v61','v63','v64','v65','v67','v68','v69',
                       'v70','v73','v76','v77','v78','v80','v81','v82','v83','v84','v85','v86','v87','v88','v89','v90','v92','v93','v94','v95','v96','v97','v98','v99',
                       'v100','v101','v102','v103','v104','v105','v106','v108','v109','v111','v115','v116','v117','v118','v119','v120','v121',
                       'v122','v123','v124','v126','v127','v128','v130','v131']

    dataset = [train, test]
    for i, st in enumerate(dataset):
        dataset[i]['max_before'] = st.max(axis=1, numeric_only=True)
        dataset[i]['min_before'] = st.min(axis=1, numeric_only=True)
        dataset[i]['mean_before'] = st.mean(axis=1, numeric_only=True)
        dataset[i]['sum_before'] = st.sum(axis=1, numeric_only=True)
        #dataset[i] = dataset[i].drop(uselessFeatures, axis=1)
        #dataset[i]['max_after'] = st.max(axis=1, numeric_only=True)
        #dataset[i]['min_after'] = st.min(axis=1, numeric_only=True)
        #dataset[i]['mean_after'] = st.mean(axis=1, numeric_only=True)
        #dataset[i]['sum_after'] = st.sum(axis=1, numeric_only=True)

    filteredTrain = dataset[0]
    filteredTest = dataset[1]
    return filteredTrain, filteredTest

def clean_data(train, test):
    for (train_name, train_series), (test_name, test_series) in zip(train.iteritems(),test.iteritems()):
        if train_series.dtype == 'O':
            #for objects: factorize
            train[train_name], tmp_indexer = pd.factorize(train[train_name])
            test[test_name] = tmp_indexer.get_indexer(test[test_name])
            #but now we have -1 values (NaN)
        else:
            #for int or float: fill NaN
            tmp_len = len(train[train_series.isnull()])
            if tmp_len>0:
                #print "mean", train_series.mean()
                train.loc[train_series.isnull(), train_name] = -999 
            #and Test
            tmp_len = len(test[test_series.isnull()])
            if tmp_len>0:
                test.loc[test_series.isnull(), test_name] = -999
    return train, test

def normalize_features(train, test):
    for (train_name, train_series), (test_name, test_series) in zip(train.iteritems(),test.iteritems()):
        minimum = train[train_name].min()
        maximum = train[train_name].max()
        a = 1.0 / (maximum - minimum)
        b = -a * minimum
        train[train_name] = a * train[train_name] + b
        test[test_name] = a * test[test_name] + b
    return train, test

def ensemble_averaging(train, test):
    g={'ne':1000,'md':30,'mf':60,'rs':2016} 
    classifiers = [
        xgb.XGBClassifier(learning_rate=0.1, n_estimators=g['ne'], max_depth=g['md'], subsample=0.8, gamma = 1),
        ExtraTreesClassifier(n_estimators=g['ne'],max_features= g['mf'],criterion= 'entropy',min_samples_split= 4, max_depth=g['md'], min_samples_leaf= 2, n_jobs = -1),
        ensemble.RandomForestClassifier(n_estimators=g['ne'], max_depth=g['md'], max_features=g['mf'], random_state=g['rs'], criterion='entropy', min_samples_split= 4, min_samples_leaf= 2, n_jobs =-1)
    ]
    predictions = np.empty(shape = (len(test), len(classifiers)))
    for i, clf in enumerate(classifiers):
        print('Training classifier ', i+1)
        clf.fit(train.values, target)
        print('Predicting on classifier ', i+1)
        predictions[:,i] = clf.predict_proba(test.values)[:, 1]
   
    final_predictions = []
    for row_number in range(len(predictions)):
        final_predictions.append(predictions[row_number, :].mean())
    return final_predictions

def ensemble_stacking(train, target, test):
    g={'ne':1000,'md':30,'mf':60,'rs':2016} 
    generalizers = [
        #GradientBoostingClassifier(learning_rate=0.1, n_estimators=g['ne']),
        ExtraTreesClassifier(n_estimators=g['ne'],max_features=g['mf'],criterion= 'entropy',min_samples_split= 4, max_depth=g['md'], min_samples_leaf= 2, n_jobs = -1),
        ensemble.RandomForestClassifier(n_estimators=g['ne'], max_depth=g['md'], max_features=g['mf'], random_state=g['rs'], criterion='entropy', min_samples_split= 4, min_samples_leaf= 2, n_jobs =-1),
        #KNeighborsClassifier(),
        #linear_model.LogisticRegression(),
    ]

    stacking_train = np.empty((train.shape[0], len(generalizers)))
    stacking_test = np.empty((test.shape[0], len(generalizers)))

    for i, alg in enumerate(generalizers):
        kf = cross_validation.KFold(len(train), n_folds=6)
        print('Training generalizer ', i+1)
        for train_idx, test_idx in kf:
            print('Starting fold..')
            stacking_train[test_idx, i] = alg.fit(train.iloc[train_idx, :], target[train_idx]).predict_proba(train.iloc[test_idx, :])[:,1]
        print('Generating stacking test...')
        stacking_test[:, i] = alg.fit(train.values, target).predict_proba(test.values)[:, 1]

    combiner = GradientBoostingClassifier(learning_rate=0.1, n_estimators=1000, max_features=len(generalizers))
    print('Training combiner...')
    combiner.fit(stacking_train, target)
    print('Predicting...')
    predictions = combiner.predict_proba(stacking_test)[:,1]
    return predictions

dirName = os.path.dirname(__file__)

print('Loading training data...')
train = pd.read_csv(os.path.join(dirName, "train.csv"))
target = train['target'].values
train = train.drop(['ID', 'target'], axis=1)

print('Loading test data...')
test = pd.read_csv(os.path.join(dirName, "test.csv"))
id_test = test['ID'].values
test = test.drop('ID', axis=1)

train, test = filter_features(train, test)

print('Cleaning data...')
train, test = clean_data(train, test)
#train, test = normalize_features(train, test)

#tuned_parameters = {
#    'loss': ['deviance'],
#    'learning_rate': [0.1],
#    'n_estimators': [1200],
#    'max_depth': [20, 30],
#    'subsample': [0.8, 1.0]
#}

#clf = GridSearchCV(GradientBoostingClassifier(), tuned_parameters, scoring='log_loss', cv=3)
#print('Fitting...')
#clf.fit(train, target)

#print('Best parameters: ', clf.best_params_)
#print()
#print('Best score: ', clf.best_score_)

g={'ne':1000,'md':30,'mf':60,'rs':2016} 
extc = ExtraTreesClassifier(n_estimators=g['ne'],max_features= g['mf'],criterion= 'entropy',min_samples_split= 4, max_depth= g['md'], min_samples_leaf= 2, n_jobs = -1)

print('Applying cross validation...')
scores = cross_validation.cross_val_score(extc, train.values, target, scoring='log_loss', cv=4)
print(scores.mean()*-1)

#print('Training...')
#extc.fit(train,target)

#print('Predicting...')
#final_predictions = extc.predict_proba(test.values)[:,1]

#final_predictions = ensemble_averaging(train, test)
#final_predictions = ensemble_stacking(train, target, test)
#pd.DataFrame({"ID": id_test, "PredictedProb": final_predictions}).to_csv('ensemble_averaging_complete_features_extra_features.csv',index=False)